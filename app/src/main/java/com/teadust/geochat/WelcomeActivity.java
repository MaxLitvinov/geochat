package com.teadust.geochat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class WelcomeActivity extends AppCompatActivity {

    private static final String USER_INFO = "userInfo";
    private static final String USER_NAME = "userName";

    private boolean isFirstTimeAppStarted;

    private EditText etName;
    private Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        isFirstTimeAppStarted = State.isFirstTimeAppStarted(this);

        if (!isFirstTimeAppStarted) {
            startActivity(new Intent(this, MapActivity.class));
            this.finish();
        }

        etName = (EditText) findViewById(R.id.et_name);
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!etName.getText().toString().equals("")) {
                    btnStart.setEnabled(true);
                } else {
                    btnStart.setEnabled(false);
                }
            }
        });

        btnStart = (Button) findViewById(R.id.btn_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = etName.getText().toString();
                UserInfo.setUserName(WelcomeActivity.this, userName);

                startActivity(new Intent(WelcomeActivity.this, MapActivity.class));
                WelcomeActivity.this.finish();
            }
        });
    }

    public static class State {

        private static final String STATE_INFO = "isFirstTimeAppStarted";
        private static final String STATE = "isFirstTimeStarted";

        public static boolean isFirstTimeAppStarted(Context context) {
            SharedPreferences statePreferences = context.getSharedPreferences(STATE_INFO, Context.MODE_PRIVATE);
            boolean isFirst = statePreferences.getBoolean(STATE, true);
            if (isFirst) {
                SharedPreferences.Editor stateEditor = statePreferences.edit();
                stateEditor.putBoolean(STATE, false);
                stateEditor.commit();
            }
            return isFirst;
        }
    }
}

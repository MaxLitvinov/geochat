package com.teadust.geochat;

import android.content.Context;
import android.content.SharedPreferences;

public class UserInfo {

    private static final String USER_INFO = "userInfo";
    private static final String USER_NAME = "userName";

    private static SharedPreferences userPreferences;
    private static SharedPreferences.Editor userEditor;

    public static void setUserName(Context context, String userName) {
        userPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        userEditor = userPreferences.edit();
        userEditor.putString(USER_NAME, userName);
        userEditor.commit();
    }

    public static String getUserName(Context context) {
        userPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        String userName = userPreferences.getString(USER_NAME, "");
        if (userName.equals("")) {
            return "No name";
        }
        return userName;
    }
}
